package me.nilober.dunklesToast.ServerCommands;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Warp {

	private String itemMaterial;
	private String name;
	private String world;
	private double x;
	private double y;
	private double z;
	private float pitch;
	private float yaw;	
	public Warp(String name, String itemMaterial, Location loc){
		this.itemMaterial = itemMaterial;
		this.name = name;
		this.world = loc.getWorld().getName();
		this.x = loc.getX();
		this.y = loc.getY();
		this.z = loc.getZ();
		this.yaw = loc.getYaw();
		this.pitch = loc.getPitch();
		
	}
	
	
	public static Location getTPLocaton(Warp w){
		return new Location(Bukkit.getWorld(w.getWorld()) ,w.getX(), w.getY(), w.getZ(), w.getYaw(), w.getPitch());
	}
	public String getItemMaterial(){
		return itemMaterial;
	}
	public String getName(){
		return name;
	}
	public String getWorld(){
		return world;
	}
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public double getZ(){
		return z;
	}
	public float getYaw(){
		return yaw;
	}
	public float getPitch(){
		return pitch;
	}
}
