package me.nilober.dunklesToast.ServerCommands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.FileConfigurationOptions;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;






@SuppressWarnings("unused")
public class Main extends JavaPlugin implements Listener {
	HashMap<String, ItemStack[]> inventory = new HashMap<>();
	int high = 11;
	int countdown;
	int cont;
	public static boolean maintenanceoption;
	public static String messageclchatlocal;
	public static String maintenancestopjoin;
	public static String messagetooklife;
	public static String bstop;
	public static String invisible;
	public static String join;
	public static String messageclchatglobal;
	public static String messageyouvisible;
	public static String messageyounotvisible;
	public static String messageinvisiblefakeleave;
	public static String messageinvisiblefakejoin;
	public static String PerformencPermissions;
	public static String YouDontHavePermissions;
	public static String invwatchpermissions;
	public static String clglobalpermissions;
	public static boolean invisiblefakejoin;
	public static boolean invisiblefakeleave;
	public static String ecwatchpermissions;
	public static String ecwatchpermissionsother;
	public static String healpermissions;
	public static String healpermissionsother;
	public static String checkpermissions;
	public static String title;
	public static String subTitle;
	public static String TitleServerName;
	public static String WarpMPermissions;
	public static int TitleTime;
	public static ChatColor TitleColor;
	public static ChatColor subTitleColor;
	public static boolean TitleAktive;
	public static boolean subTitleAktiv;
	public static String prefix = ChatColor.translateAlternateColorCodes('&', "&7[&bServerCMDS&7] &4");
	ArrayList<Player> visible = new ArrayList<Player>();
	public List<Warp> warps = new ArrayList<Warp>();
	public static Main getInstance(){
		return instance;
    }
	public Runtime runtime;
	public static Main instance;   
	private Gson gson;
	//ON-ENABLE
	public void onEnable(){
		getServer().getPluginManager().registerEvents(this, this);
		getCommand("warp").setExecutor(new WarpCommand(this));
		Config.setConfig();
		Config.reloadConfig();
		Config.saveConfig();
		Bukkit.broadcastMessage(this.prefix + "The Plugin was succesfully loaded");
    	instance = this;
    	runtime = Runtime.getRuntime();
    	Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Lag(), 100L, 1L);

    	Bukkit.getPluginManager().registerEvents(new InvListener(this), this);
    	this.gson = new Gson();
    	getDataFolder().mkdir();
    	if (!getFile().exists()){
    		try {
				getFile().createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	} else {
    		try {
    			FileReader reader = new FileReader(getFile());
    			BufferedReader buff = new BufferedReader(reader);
    			StringBuilder fileContent = new StringBuilder();
    			String line = null;
    			while ((line = buff.readLine()) != null){
    				fileContent.append(line);
    			}
    			String content = fileContent.toString();
    			WarpCollection warps = gson.fromJson(content, WarpCollection.class);
    			for (Warp w : warps.getWarps()){
    				this.warps.add(w);
    			}
    			buff.close();
    			reader.close();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    	
	}
	
	@Override
	protected File getFile(){
		return new File(getDataFolder(), "warps.json");
	}
	@Override
	public void onDisable(){
		WarpCollection warpCollection = new WarpCollection(this.warps);
		String json = gson.toJson(warpCollection);
		try {
			FileWriter writer = new FileWriter(getFile());
			writer.write(json);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	//MAINTENANCE
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		if(Main.maintenanceoption == true){
			Player p = e.getPlayer();
			if (!p.hasPermission(Main.join)) {
				p.kickPlayer(ChatColor.translateAlternateColorCodes('&', Main.maintenancestopjoin));
			}
  		}
		if (visible.contains(e.getPlayer())){
			for (Player online : Bukkit.getOnlinePlayers()){
				online.hidePlayer(e.getPlayer());
			}
			e.setJoinMessage(null);
		}
		if (this.TitleAktive == true){
			if (this.subTitleAktiv == true){
				Title title = new Title(this.title.replaceAll("%SERVERNAME", this.TitleServerName), this.subTitle.replaceAll("%SERVERNAME", this.TitleServerName));
				title.setTitleColor(this.TitleColor);
				title.setSubtitleColor(this.subTitleColor);
				title.send(e.getPlayer());
				title.setStayTime(this.TitleTime);
				title.setTimingsToSeconds();
				
			} else {
				Title title = new Title(this.title.replaceAll("%SERVERNAME", this.TitleServerName));
				title.setTitleColor(this.TitleColor);
				title.send(e.getPlayer());
				title.setStayTime(this.TitleTime);
				title.setTimingsToSeconds();
			}
		}
	}
	//Left
	@EventHandler
	public void onLeft(PlayerQuitEvent e) {
		if (visible.contains(e.getPlayer())){
			e.setQuitMessage(null);
		}
	}
	//COMMANDS
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String cmdlabel, String[] args){
		Player p = (Player)sender;
		if (cmd.getName().equalsIgnoreCase("kill")){
			p.sendMessage(Main.messagetooklife);
			p.setHealth(0);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("invwatch")){
			if(args.length == 1) {
				if (p.hasPermission(this.invwatchpermissions)){
					Player target = Bukkit.getPlayer(args[0]);
					p.openInventory(target.getInventory());
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix + "&cYou�re now looking in the Inventory from: &e" + target.getName()));
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.YouDontHavePermissions));
				}
				
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix + "&eUse: &c/invwatch <target_name>"));
			}
			return true;
		}	
		if (cmd.getName().equalsIgnoreCase("ecwatch")){
			if(args.length == 1) {
				if (p.hasPermission(this.ecwatchpermissionsother)){
					Player target = Bukkit.getPlayer(args[0]);
					p.openInventory(target.getEnderChest());
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix + "&cYou�re now looking in the EnderChest from: &e" + target.getName()));
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.YouDontHavePermissions));
				}
				
			} else {
				if (p.hasPermission(this.ecwatchpermissions)){
					p.openInventory(p.getEnderChest());
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix + "&cYou�re now looking in the EnderChest from: &eYour own"));
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.YouDontHavePermissions));
				}
			}
			return true;
		}	
		if (cmd.getName().equalsIgnoreCase("check")){
			if(args.length == 1) {
				if (p.hasPermission(this.checkpermissions)){
					Player target = Bukkit.getPlayer(args[0]);
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',("&f----" + this.prefix + "&f----")));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',("&6Checking:&e " + target.getDisplayName())));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',("&6Is OP:&e " + target.isOp())));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',("&6Is flying:&e " + target.isFlying())));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',("&6Health:&e " + target.getHealthScale())));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',("&6Potions:&e " + target.getActivePotionEffects())));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',("&6IP:&e " + target.getAddress().getAddress())));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',("&6Gamemode:&e " + target.getGameMode())));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6UUID:&e " + target.getUniqueId()));
					//OPEN PLAYER INVENTORY
					TextComponent vorder = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&6Open Player Inventory:&e "));
					TextComponent message = new TextComponent("Click me!");
					message.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
					message.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/invwatch " + target.getName()));
					message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', "&4Open the Inventory of &e" + target.getName() + "&4!")).create()));
					vorder.addExtra(message);
					p.spigot().sendMessage(vorder);
					//OPEN PLAYER INVENTORY ENDE
					//OPEN ENDER CHEST
					TextComponent openendvorder = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&6Open Player EnderChest:&e "));
					TextComponent openender = new TextComponent("Click me!");
					openender.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
					openender.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/ecwatch " + target.getName()));
					openender.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', "&4Open the EnderChest from &e" + target.getName() + "&4!")).create()));
					
					openendvorder.addExtra(openender);
					p.spigot().sendMessage(openendvorder);
					//CLEAR EFFECTS
					TextComponent clearvorder = new TextComponent(ChatColor.translateAlternateColorCodes('&', "&6Clear All Effects:&e "));
					TextComponent clear = new TextComponent("Click me!");
					clear.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
					clear.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/effect " + target.getName() + " clear"));
					clear.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', "&4Clear all Effects from &e" + target.getName() + "&4!")).create()));
					clearvorder.addExtra(clear);
					p.spigot().sendMessage(clearvorder);
					//CLEAR EFFECTS ENDE
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',("&f----" + this.prefix + "&f----")));
					
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.YouDontHavePermissions));
				}
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',this.prefix + "use: /check <player_name>"));
			}
		}
		if (cmd.getName().equalsIgnoreCase("testcommand")){
			if (args.length == 1){
				TextComponent message = new TextComponent("Test Button!");
				message.setClickEvent(new ClickEvent(this.openEnderChest(Bukkit.getPlayer(args[0]), p), ""));
				message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Das ist ein Text Command!").create()));
				p.spigot().sendMessage(message);
			}
		}
		if (cmd.getName().equalsIgnoreCase("inhand")){
			if(args.length == 1) {
				Player target = Bukkit.getPlayer(args[0]);
				p.getItemInHand();
				p.sendMessage(this.prefix + "�cHas this in his Hand:: �e" + target.getItemInHand());
			}
			else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUse: &e/inhand <target_name>"));
				
			}
			return true;
		}
			
			
		if (cmd.getName().equalsIgnoreCase("heal")){
			if(args.length == 1) {
				if (p.hasPermission(this.healpermissionsother)){
					Player target = Bukkit.getPlayer(args[0]);
					target.setHealth(20);
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix + "&cYou have succesfully healed: &e" + target.getName()));
					target.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix + "You were healed by " + p.getName()));
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.YouDontHavePermissions));
				}
				
			} else {
				if (p.hasPermission(this.healpermissions)){
					p.setHealth(20);
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix + "&cYou have succesfully healed: &eYou!"));
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.YouDontHavePermissions));
				}
			}
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("cllocal")){
			Message.clearChat(p);
			return true;
        }
		if (cmd.getName().equalsIgnoreCase("clglobal")){
			if (p.hasPermission(this.clglobalpermissions)){
				Message.clearChatGlobal();
				return true;
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.YouDontHavePermissions));
			}
		}
		if (cmd.getName().equalsIgnoreCase("perform")){
			if (p.hasPermission(Main.PerformencPermissions)){
				double tps = Lag.getTPS();
				
				if (tps > 19){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eServer Performance:"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eMax. Memory: &4" + this.runtime.maxMemory() / 1048576L + " &eMB"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eFree Memory: &4" + this.runtime.freeMemory() / 1048576L + " &eMB"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eTPS: &a" + tps + "&e/&a20"));
				} else if (tps > 18.5){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eServer Performance:"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eMax. Memory: &4" + this.runtime.maxMemory() / 1048576L + " &eMB"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eFree Memory: &4" + this.runtime.freeMemory() / 1048576L + " &eMB"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eTPS: &e" + tps + "&e/&a20"));
				} else if (tps < 18.5){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eServer Performance:"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eMax. Memory: &4" + this.runtime.maxMemory() / 1048576L + " &eMB"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eFree Memory: &4" + this.runtime.freeMemory() / 1048576L + " &eMB"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eTPS: &4" + tps + "&e/&a20"));
				}
				
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.YouDontHavePermissions));
			}
		}
		if (cmd.getName().equalsIgnoreCase("bstop")) {
			if (args.length == 1){
				if (p.hasPermission(Main.bstop)){
					try{
						this.cont = Integer.parseInt(args[0]) + 1;
					} catch (Exception ex){
						this.cont = 11;
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix + "You must specifying a number!"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix + "Start Countdown with standard time!"));
					}
					
					
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&e [ServerCMDS] The Server is going for shutdown in " + this.cont + " Seconds!"));
					this.countdown = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
						public void run(){
							if (cont >= 1){
								cont--;
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.this.prefix + cont + " &6Seconds remaining"));

							}
							if (cont == 0){
								Bukkit.broadcastMessage("Server is going for shutdown!");
								Bukkit.savePlayers();
								Bukkit.shutdown();
								Bukkit.getScheduler().cancelTask(Main.this.countdown);
							}
						}
					}, 0, 20);
					return true;
					
				}
			} else if (args.length == 0){
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&e [ServerCMDS] The Server is going for shutdown in " + this.high + " Seconds!"));
				this.countdown = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
					public void run(){
						if (Main.this.high >= 1){
							Main.this.high--;
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.this.prefix + Main.this.high + " &6Seconds remaining"));

						}
						if (Main.this.high == 0){
							Bukkit.broadcastMessage("Server is going for shutdown!");
							Bukkit.savePlayers();
							Bukkit.shutdown();
							Bukkit.getScheduler().cancelTask(Main.this.countdown);
						}
					}
				}, 0, 20);
				return true;
			}
			
			
		}
		if (cmd.getName().equalsIgnoreCase("invisible")) {
			if (p.hasPermission(Main.invisible)){
    		 	if (visible.contains(p)){
    		 		visible.remove(p);
    		 		p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.prefix + Main.messageyounotvisible));
    		 		p.removePotionEffect(PotionEffectType.INVISIBILITY);
        		 	for (Player online : Bukkit.getOnlinePlayers()){
        		 		online.showPlayer(p);
        		 	}
        		 	if (Main.invisiblefakejoin == true){
        		 		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', Main.messageinvisiblefakejoin).replaceAll("%PLAYER", p.getName()));
        		 	}
    		 	} else {
        		 	visible.add(p);
        		 	p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 100000, 100000), false);
        		 	for (Player online : Bukkit.getOnlinePlayers()){
        		 		online.hidePlayer(p);
        		 	}
        		 	p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.prefix + Main.messageyouvisible));
        		 	if (Main.invisiblefakeleave == true){
        		 		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', Main.messageinvisiblefakeleave).replaceAll("%PLAYER", p.getName()));
        		 	}
    		 	}
    		 	
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.YouDontHavePermissions));
			}
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("warpm")){
			if (p.hasPermission(Main.WarpMPermissions)){
				
				Inventory inv = Bukkit.createInventory(null, 1*9, "WarpM");
				//SetWarp Item
				ItemStack setwarp = new ItemStack(Material.COMPASS);
				ItemMeta setwarpmeta = setwarp.getItemMeta();
				setwarpmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&',"&2Create a new Warp Point!"));
				setwarp.setItemMeta(setwarpmeta);
				//DelWarp item
				ItemStack delwarp = new ItemStack(Material.BARRIER);
				ItemMeta delwarpmeta = delwarp.getItemMeta();
				delwarpmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4Delete a Warp Point!"));
				delwarp.setItemMeta(delwarpmeta);
				//Change Icon
				ItemStack changeIcon = new ItemStack(Material.GRASS);
				ItemMeta changeIconMeta = changeIcon.getItemMeta();
				changeIconMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&bChange the icon from a Warp Point!"));
				changeIcon.setItemMeta(changeIconMeta);
				//AddItem
				inv.addItem(setwarp);
				inv.addItem(delwarp);
				inv.addItem(changeIcon);
				p.openInventory(inv);
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.YouDontHavePermissions));
				
			}
			
		}
		
		
		return true;
    }
  
	private ClickEvent Test() {
		System.out.println("Test");
		return null;
	}
	public Action openEnderChest(Player target, Player p){
		p.openInventory(target.getEnderChest());
		return Action.valueOf("Test");

		
	}
	public int getWarpInventorySize(int warps){
		return (int) Math.ceil(warps / 9.0D);
	}
	
}
