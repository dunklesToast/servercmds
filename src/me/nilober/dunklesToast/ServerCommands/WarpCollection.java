package me.nilober.dunklesToast.ServerCommands;

import java.util.List;

public class WarpCollection {

	private List<Warp> warps;
	public WarpCollection(List<Warp> warps){
		this.warps = warps;
	}
	public List<Warp> getWarps(){
		return warps;
	}
	public void setWarps(List<Warp> warps){
		this.warps = warps;
	}
}
