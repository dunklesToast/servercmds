package me.nilober.dunklesToast.ServerCommands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

public class InvListener implements Listener {

	
	public HashMap<Player, ItemStack> inv = new HashMap<Player, ItemStack>();
	public HashMap<Player, String> warpnames = new HashMap<Player, String>();
	public List<Player> tele = new ArrayList<Player>();
	public List<Block> bl  = new ArrayList<Block>();
	
	private Main plugin;
	public InvListener(Main plugin){
		this.plugin = plugin;
	}
	@EventHandler
	public void onClick(InventoryClickEvent e){
		HumanEntity p = e.getWhoClicked();
		if (e.getInventory() != null){
			if (e.getInventory().getName() == "WarpM"){
				e.setCancelled(true);
				if (e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.COMPASS){
					ItemStack book = new ItemStack(Material.BOOK_AND_QUILL);
					BookMeta bookmeta = (BookMeta) book.getItemMeta();
					ArrayList<String> lore = new ArrayList<String>();
					lore.add("Signed this book with the");
					lore.add("name from the new WarpPoint.");
					bookmeta.setLore(lore);
					bookmeta.addPage(ChatColor.translateAlternateColorCodes('&', "&4Please sign this book with the new warp name."));
					bookmeta.addPage("Please ignore this!");
					bookmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4&lCreat a new Warp-Point!"));
					book.setItemMeta(bookmeta);
					
					inv.put((Player) p, p.getInventory().getItem(0));
					p.getInventory().setItem(0, book);
					
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',plugin.prefix + "Pleas write in the book and quill the name from the new WarpPoint."));
					p.closeInventory();
				}
				if (e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.BARRIER){
					ItemStack book = new ItemStack(Material.BOOK_AND_QUILL);
					BookMeta bookmeta = (BookMeta) book.getItemMeta();
					ArrayList<String> lore = new ArrayList<String>();
					lore.add("Sign this book with the name");
					lore.add("which you want to delet.");
					bookmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4Sign this book with the name from the warp point which you want to delet."));
					bookmeta.addPage(ChatColor.translateAlternateColorCodes('&', "&4Please sign this book with the new warp name."));
					bookmeta.addPage("Please ignore this!");
					book.setItemMeta(bookmeta);
					inv.put((Player) e.getWhoClicked(), e.getWhoClicked().getInventory().getItem(0));
					e.getWhoClicked().getInventory().setItem(0, book);
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',plugin.prefix +  "Sign this book with the warp name."));
					p.closeInventory();
				}
				if (e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.GRASS){
	
					ItemStack book = new ItemStack(Material.BOOK_AND_QUILL);
					BookMeta bookmeta = (BookMeta) book.getItemMeta();
					bookmeta.addPage("Test");
					bookmeta.setDisplayName("Test");
					book.setItemMeta(bookmeta);
					
					p.getInventory().addItem(book);
					
				}
			}
			if (e.getCurrentItem() != null && e.getInventory().getName() == "Warps!"){
				HashMap<Location, Material> bll = new HashMap<Location, Material>();
				HashMap<Location, Byte> bita = new HashMap<Location, Byte>();
				List<Location> loclist = new ArrayList<Location>();
				e.setCancelled(true);
				ItemStack is = e.getCurrentItem();
				String warpname = ChatColor.stripColor(is.getItemMeta().getDisplayName());
				List<Warp> warps = plugin.warps.stream().filter((warp) -> warp.getName().equalsIgnoreCase(warpname)).collect(Collectors.toList());
				tele.add((Player) p);
				Location pl = p.getLocation();

				Location genau = new Location(pl.getWorld(), pl.getBlockX(), pl.getBlockY(), pl.getBlockZ());
				
				
				for (int ii = 0; ii < 256; ii++){
					Location cu = new Location(pl.getWorld(), pl.getX(), ii, pl.getZ());
					bll.put(cu, cu.getBlock().getType());
					bita.put(cu, cu.getBlock().getData());
					//loclist.set(ii, cu);
					loclist.add(cu);
					if (ii > pl.getBlockY()){
						cu.getBlock().setType(Material.AIR);
					}
					
				}
				
			
				p.closeInventory();
				if (warps.size() > 0){
					p.setVelocity(new Vector(0.0, 4.0 , 0.0));
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
						@Override
						public void run() {
							p.teleport(Warp.getTPLocaton(warps.get(0)));
							tele.remove((Player)p);
							for (int i = 0; i < 256; i++){
								loclist.get(i).getBlock().setType(bll.get(loclist.get(i)));
								loclist.get(i).getBlock().setData(bita.get(loclist.get(i)));
							}
							for (int i = 0; i < 256; i++){
								bll.clear();
								bita.clear();
								loclist.clear();
							}
							Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
								@SuppressWarnings("deprecation")
								@Override
								public void run() {
									((Player) p).playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 2, 2);
									for (int i = 0; i < 100; i++){
										((Player)p).playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 100);
									}
									
								}
								
							}, 5L);
							
						}
						
					}, 2*15L);
					Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){

						@SuppressWarnings("deprecation")
						@Override
						public void run() {
							if (tele.contains((Player) p)){
								((Player)p).playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 100);
								((Player)p).playEffect(p.getLocation(), Effect.STEP_SOUND, 368);
							}
							
						}
						
					}, 1L, 1L);
				}
			}
			if (e.getCurrentItem() != null && e.getInventory().getName() == "You have no warps"){
				e.setCancelled(true);
			}
			if (e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.BOOK_AND_QUILL){
				if (inv.containsKey((Player) p)){
					e.setCancelled(true);
					e.getCurrentItem().setType(Material.AIR);
					p.getInventory().setItem(0, inv.get(p));
					inv.remove(p);
				}
			}
			
			
		}
	}
	@EventHandler
	public void onSigned(PlayerEditBookEvent e){
		ItemMeta old = e.getPreviousBookMeta();
		ItemMeta newm = e.getNewBookMeta();
		
		BookMeta ha = (BookMeta) newm;
			ha.addPage("Not");
			if (old.getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4&lCreat a new Warp-Point!"))){
				BookMeta b = (BookMeta) newm;
				if (inv.containsKey(e.getPlayer())){
					e.getPlayer().sendMessage(b.getTitle());
					String warpname = b.getTitle();
					ItemStack book = new ItemStack(Material.BOOK_AND_QUILL);
					BookMeta bookmeta = (BookMeta) book.getItemMeta();
					bookmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4&lSigned the book with an Material."));
					ArrayList<String> lore = new ArrayList<String>();
					lore.add("Signed this book with");
					lore.add("an Material, for the Warp");
					lore.add("Icon.");
					bookmeta.addPage(ChatColor.translateAlternateColorCodes('&', "&4This Book you must sign with an Material."));
					bookmeta.addPage("Please ignore this.");
					bookmeta.setLore(lore);
					book.setItemMeta(bookmeta);
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
						@Override
						public void run() {
							e.getPlayer().getInventory().setItem(0, book);
						}
					}, 5L);
					warpnames.put(e.getPlayer(), warpname);
				}
					
				
			}
			if (old.getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4&lSigned the book with an Material."))){
				if (inv.containsKey(e.getPlayer())){
					BookMeta b = (BookMeta) newm;
					if (b.getPageCount() <= 0){
						b.addPage("This Text is not important!");
					} else {
						String warpname = warpnames.get(e.getPlayer());
						BookMeta b1 = (BookMeta) newm;
						String material = b1.getTitle();
						if (plugin.warps.stream().filter((warp) -> warp.getName().equalsIgnoreCase(warpname)).count() > 0){
							e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "This Warp is already exist!"));
							
						}
						if (Material.valueOf(material) == null){
							e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "This is not an material!"));
						} else {
							Material mat = Material.valueOf(material);
							plugin.warps.add(new Warp(warpname, material, e.getPlayer().getLocation()));
							e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "The warp is succesful created."));
							e.getPlayer().getInventory().setItem(0, inv.get(e.getPlayer()));
							inv.remove(e.getPlayer());
							warpnames.remove(e.getPlayer());
						}
					}
					
				}
			}
			if (old.getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4Sign this book with the name from the warp point."))){
				if (inv.containsKey(e.getPlayer())){
					BookMeta b = (BookMeta) newm;
					String warpname = b.getTitle();
					List<Warp> warps = plugin.warps.stream().filter((warp) -> warp.getName().equalsIgnoreCase(warpname)).collect(Collectors.toList());
					if (warps.size() <= 0){
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "This warp is not exists."));
						e.getPlayer().getInventory().setItem(0, inv.get(e.getPlayer()));
						inv.remove(e.getPlayer());
					} else {
						plugin.warps.remove(warps.get(0));
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "You habe removed the warp " + warpname + "."));
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){

							@Override
							public void run() {
								e.getPlayer().getInventory().setItem(0, inv.get(e.getPlayer()));
								inv.remove(e.getPlayer());
								
							}
							
						}, 5L);
					}
				}
			}
		
			
			if (old.getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4&lSigned the book with an Material."))){
				if (inv.containsKey(e.getPlayer())){
					BookMeta b = (BookMeta) newm;
					if (b.getPageCount() <= 0){
						b.addPage("This Text is not important!");
					} else {
						String warpname = warpnames.get(e.getPlayer());
						BookMeta b1 = (BookMeta) newm;
						String material = b1.getTitle();
						if (plugin.warps.stream().filter((warp) -> warp.getName().equalsIgnoreCase(warpname)).count() > 0){
							e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "This Warp is already exist!"));
							
						}
						if (Material.valueOf(material) == null){
							e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "This is not an material!"));
						} else {
							Material mat = Material.valueOf(material);
							plugin.warps.add(new Warp(warpname, material, e.getPlayer().getLocation()));
							e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "The warp is succesful created."));
							e.getPlayer().getInventory().setItem(0, inv.get(e.getPlayer()));
							inv.remove(e.getPlayer());
							warpnames.remove(e.getPlayer());
						}
					}
					
				}
			}
			if (old.getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4Sign this book with the name from the warp point."))){
				if (inv.containsKey(e.getPlayer())){
					BookMeta b = (BookMeta) newm;
					String warpname = b.getTitle();
					List<Warp> warps = plugin.warps.stream().filter((warp) -> warp.getName().equalsIgnoreCase(warpname)).collect(Collectors.toList());
					if (warps.size() <= 0){
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "This warp is not exists."));
					} else {
						plugin.warps.remove(warps.get(0));
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.prefix + "You habe removed the warp " + warpname + "."));
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){

							@Override
							public void run() {
								e.getPlayer().getInventory().setItem(0, inv.get(e.getPlayer()));
								inv.remove(e.getPlayer());
								
							}
							
						}, 5L);
					}
				
				}
			}
		
		
		
		
	}
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		if (inv.containsKey(e.getPlayer())){
			e.getPlayer().getInventory().setItem(0, inv.get(e.getPlayer()));
			inv.remove(e.getPlayer());
		}
	}
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		if (inv.containsKey(e.getPlayer())){
			e.setCancelled(true);
			e.getPlayer().getInventory().setItem(0, new ItemStack(Material.AIR));
			e.getPlayer().getInventory().setItem(0, inv.get(e.getPlayer()));
			inv.remove(e.getPlayer());
			e.getItemDrop().remove();
			
		}
	}
	
	
}
