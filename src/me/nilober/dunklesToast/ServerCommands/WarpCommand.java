package me.nilober.dunklesToast.ServerCommands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

public class WarpCommand implements CommandExecutor{

	public List<Player> tele = new ArrayList<Player>();
	private Main plugin;
	public WarpCommand(Main plugin){
		this.plugin = plugin;
	}
	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("warp")){
			if (args.length > 0 && args.length < 2){
				if (!(s instanceof Player)){
					return true;
				}
				HashMap<Location, Material> bll = new HashMap<Location, Material>();
				HashMap<Location, Byte> bita = new HashMap<Location, Byte>();
				List<Location> loclist = new ArrayList<Location>();
				Player p = (Player)s;
				Location pl = p.getLocation();
				String warpname = args[0];
				tele.add((Player) p);
				for (int ii = 0; ii < 256; ii++){
					Location cu = new Location(pl.getWorld(), pl.getX(), ii, pl.getZ());
					bll.put(cu, cu.getBlock().getType());
					bita.put(cu, cu.getBlock().getData());
					//loclist.set(ii, cu);
					loclist.add(cu);
					if (ii > pl.getBlockY()){
						cu.getBlock().setType(Material.AIR);
					}
					
				}
				Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){

					@SuppressWarnings("deprecation")
					@Override
					public void run() {
						if (tele.contains((Player) p)){
							((Player)p).playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 100);
							((Player)p).playEffect(p.getLocation(), Effect.STEP_SOUND, 368);
						}
						
					}
					
				}, 1L, 1L);
				plugin.warps.forEach(new Consumer<Warp>(){
					@Override
					public void accept(Warp w) {
						if (warpname.equalsIgnoreCase(w.getName())){
							p.setVelocity(new Vector(p.getLocation().getDirection().getY(), 4d, p.getLocation().getDirection().getZ()));
							Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
								@Override
								public void run() {
									p.teleport(Warp.getTPLocaton(w));
									tele.remove((Player)p);
									for (int i = 0; i < 256; i++){
										loclist.get(i).getBlock().setType(bll.get(loclist.get(i)));
										loclist.get(i).getBlock().setData(bita.get(loclist.get(i)));
									}
									for (int i = 0; i < 256; i++){
										bll.clear();
										bita.clear();
										loclist.clear();
									}
									Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
										@Override
										public void run() {
											p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 2, 2);
											
											for (int i = 0; i < 100; i++){
												p.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 100);
											}
										}
										
									}, 5L);
									
								}
								
							}, 2*15L);
							return;
						}
						
					}
					
				});
			}
			if (args.length == 0){
				
				if (plugin.getWarpInventorySize(plugin.warps.size()) * 9 == 0){
					Inventory warpinv2 = Bukkit.createInventory(null, 9, "You have no warps");
					ItemStack is = new ItemStack(Material.BARRIER);
					ItemMeta im = is.getItemMeta();
					im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4You have no warps!"));
					is.setItemMeta(im);
					warpinv2.addItem(is);
					Player p = (Player)s;
					p.openInventory(warpinv2);
				} else {
					Inventory warpinv = Bukkit.createInventory(null, plugin.getWarpInventorySize(plugin.warps.size()) * 9, "Warps!");
					for (Warp w : plugin.warps){
						
						ItemStack is = new ItemStack(Material.valueOf(w.getItemMaterial()));
						if (is.getType() == Material.AIR){
							ItemStack isbed = new ItemStack(Material.BEDROCK);
							ItemMeta isbedmeta = isbed.getItemMeta();
							isbedmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4" + w.getName()));
							isbed.setItemMeta(isbedmeta);
							warpinv.addItem(isbed);
							Player p = (Player)s;
							p.openInventory(warpinv);
						} else {
							ItemMeta im = is.getItemMeta();
							im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4" + w.getName()));
							is.setItemMeta(im);
							warpinv.addItem(is);
							Player p = (Player)s;
							p.openInventory(warpinv);
						}
						
						
					}
				}
				
				
			}
		}
		
		
		return true;
	}

}
