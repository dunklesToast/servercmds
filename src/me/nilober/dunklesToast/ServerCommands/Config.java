package me.nilober.dunklesToast.ServerCommands;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {
    
    
    public static File getConfigFile(){
		return new File("plugins/ServerCMDS" , "config.yml");
	}
	public static FileConfiguration getConfigFileConfiguration(){
		return YamlConfiguration.loadConfiguration(getConfigFile());
	}
	public static void saveConfig() {
		try {
			getConfigFileConfiguration().save(getConfigFile());
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	public static void setConfig(){
		FileConfiguration cfg = getConfigFileConfiguration();
        cfg.options().copyDefaults(true);
        cfg.options().header("Server Commands - Config - For ColorCodes please '&'!");
        //Maintenance
        cfg.addDefault("scmds.maintenance.messages.WereCurrentlyinMaintenance", "We�re currently in &4Maintenance�f - &aPlease come back later");
        cfg.addDefault("scmds.maintenance.options.MaintenanceModeEnable", false);
        cfg.addDefault("scmds.maintenance.permissions.CanJoinAtMaintenance", "ServerCMDS.Join");
		//Clear Chat
        cfg.addDefault("scmds.clchat.messages.local", "Your chat was succefully cleared!");
		cfg.addDefault("scmds.clchat.messages.Global", "The Global Chat was succefully cleared!");
		//Suicide
		cfg.addDefault("scmds.suicide.messages.tooklife", "Good by world...");
		//bstop
        cfg.addDefault("scmds.bstop.permissions", "ServerCMDS.CanStopServer");
        //Invisible
        cfg.addDefault("scmds.invisible.permissions", "ServerCMDS.Invisible");
        cfg.addDefault("scmds.invisible.message.Invisible", "Your are now Invisible!");
        cfg.addDefault("scmds.invisible.message.NoInvisible", "You aren't longer Invisible!");
        cfg.addDefault("scmds.invisible.options.FakeLeave", true);
        cfg.addDefault("scmds.invisible.options.FakeJoin", true);
        cfg.addDefault("scmds.invisible.message.FakeLeaveMessage", "&e%PLAYER left the game.");
        cfg.addDefault("scmds.invisible.message.FakeJoinMessage", "&e%PLAYER joined the game.");
        //Performenc
        cfg.addDefault("scmds.performenc.permissions.CanSeePerformenc", "ServerCMDS.Performenc");
        //invwatch
        cfg.addDefault("scmds.invwatch.permissions.PermissionsToUse", "ServerCMDS.Invwatch");
        //ecwatch
        cfg.addDefault("scmds.ecwatch.permissions.PermissionsToUse", "ServerCMDS.ecwatch");
        cfg.addDefault("scmds.ecwatch.permissions.PermissionsToSeeOther", "ServerCMDS.ecwatch.other");
        //heal
        cfg.addDefault("scmds.heal.permissions.PermissionsToUse", "ServerCMDS.Heal");
        cfg.addDefault("scmds.heal.permissions.PermissionsToHealOther", "ServerCMDS.Heal.Other");
        //YouDontHavePermissions
        cfg.addDefault("scmds.message.YouDontHavePermissions", "&4You Dont have Permissions to do this!");
        //clglobal
        cfg.addDefault("scmds.clglobal.permissions.PermissionsToUse", "ServerCMDS.clglobal");
        //check
        cfg.addDefault("scmds.check.permissions.Check", "ServerCMDS.Check");
        //Server Welcome Message
        cfg.addDefault("scmds.WelcomeMessage.option.Activate", true);
        cfg.addDefault("scmds.WelcomeMessage.option.SubTitle", false);
        cfg.addDefault("scmds.WelcomeMessage.message.Title", "Welcome on %SERVERNAME!");
        cfg.addDefault("scmds.WelcomeMessage.message.SubTitle", "Change this in the Config.yml");
        cfg.addDefault("scmds.WelcomeMessage.option.ServerName", "Default");
        cfg.addDefault("scmds.WelcomeMessage.option.TitleColor", "YELLOW");
        cfg.addDefault("scmds.WelcomeMessage.option.SubTitleColor", "RED");
        cfg.addDefault("scmds.WelcomeMessage.option.Time", 2);
        //WarpM
        cfg.addDefault("scmds.Warp.permissions.WarpM", "ServerCMDS.WarpM");
        cfg.options().copyDefaults(true);
        try {
			cfg.save(getConfigFile());
			System.out.println("[ServerCMDS] Config.yml was set!");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    public static void reloadConfig(){
        FileConfiguration cfg = getConfigFileConfiguration();
        Main.getInstance();
        //Maintenance
        Main.maintenancestopjoin = cfg.getString("scmds.maintenance.messages.WereCurrentlyinMaintenance");
        Main.maintenanceoption = cfg.getBoolean("scmds.maintenance.options.MaintenanceModeEnable");
        Main.join = cfg.getString("scmds.maintenance.messages.WereCurrentlyinMaintenancen");
        //Clear Chat
        Main.messageclchatlocal = cfg.getString("scmds.clchat.messages.local");
        Main.messageclchatglobal = cfg.getString("scmds.clchat.messages.Global");
        //Suicide
        Main.messagetooklife = cfg.getString("scmds.suicide.messages.tooklife");
        //bstop
        Main.bstop = cfg.getString("scmds.bstop.permissions");
        //Invisible
        Main.invisible = cfg.getString("scmds.invisible.permissions");
        Main.messageyouvisible = cfg.getString("scmds.invisible.message.Invisible");
        Main.messageyounotvisible = cfg.getString("scmds.invisible.message.NoInvisible");
        Main.invisiblefakeleave = cfg.getBoolean("scmds.invisible.options.FakeLeave");
        Main.messageinvisiblefakeleave = cfg.getString("scmds.invisible.message.FakeLeaveMessage");
        Main.messageinvisiblefakejoin = cfg.getString("scmds.invisible.message.FakeJoinMessage");
		Main.invisiblefakejoin = cfg.getBoolean("scmds.invisible.options.FakeJoin");
        //Perform
		Main.PerformencPermissions = cfg.getString("scmds.performenc.permissions.CanSeePerformenc");
		//Plugin
		Main.YouDontHavePermissions = cfg.getString("scmds.message.YouDontHavePermissions");
		//invwatch
		Main.invwatchpermissions = cfg.getString("scmds.invwatch.permissions.PermissionsToUse");
		//ecwatch
		Main.ecwatchpermissions = cfg.getString("scmds.ecwatch.permissions.PermissionsToUse");
		Main.ecwatchpermissionsother = cfg.getString("scmds.ecwatch.permissions.PermissionsToSeeOther");
		//heal
		Main.healpermissions = cfg.getString("scmds.heal.permissions.PermissionsToUse");
		Main.healpermissionsother = cfg.getString("scmds.heal.permissions.PermissionsToHealOther");
		//clglobal
		Main.clglobalpermissions = cfg.getString("scmds.clglobal.permissions.PermissionsToUse");
		//Check
		Main.checkpermissions = cfg.getString("scmds.check.permissions.Check");
		//Welcome Message
		Main.title = cfg.getString("scmds.WelcomeMessage.message.Title");
		Main.subTitle = cfg.getString("scmds.WelcomeMessage.message.SubTitle");
		Main.subTitleAktiv = cfg.getBoolean("scmds.WelcomeMessage.option.SubTitle");
		Main.TitleAktive = cfg.getBoolean("scmds.WelcomeMessage.option.Activate");
		Main.TitleServerName = cfg.getString("scmds.WelcomeMessage.option.ServerName");
		Main.TitleColor = ChatColor.valueOf(cfg.getString("scmds.WelcomeMessage.option.TitleColor"));
		Main.subTitleColor = ChatColor.valueOf(cfg.getString("scmds.WelcomeMessage.option.SubTitleColor"));
		Main.TitleTime = cfg.getInt("scmds.WelcomeMessage.option.Time");
		//warpM
		Main.WarpMPermissions = cfg.getString("scmds.Warp.permissions.WarpM");
		
		System.out.println("[ServerCMDS] Config.yml Reloaded!");
		
        
    }
    
}